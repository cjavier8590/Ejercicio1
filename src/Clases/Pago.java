package Clases;

import java.util.Date;

/**
 * Created by Javier Lopez on 4/07/2017.
 */
public class Pago {

    public String noPago;
    public String banco;
    public String montoPago;
    public Date fechaPago;

    public Pago(String noPago, String banco, String montoPago, Date fechaPago) {
        this.noPago = noPago;
        this.banco = banco;
        this.montoPago = montoPago;
        this.fechaPago = fechaPago;
    }

    public Pago() {
    }

    public String getNoPago() {
        return noPago;
    }

    public void setNoPago(String noPago) {
        this.noPago = noPago;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getMontoPago() {
        return montoPago;
    }

    public void setMontoPago(String montoPago) {
        this.montoPago = montoPago;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }
}
