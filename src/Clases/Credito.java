package Clases;

import java.util.List;

/**
 * Created by Javier Lopez on 3/07/2017.
 */
public class Credito {

    public String monto;
    public double interes;
    public long fechaInicio;
    public long fechaFin;

    public List<Pago> pagos;

    public Credito() {
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public double getInteres() {
        return interes;
    }

    public void setInteres(double interes) {
        this.interes = interes;
    }

    public long getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(long fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public long getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(long fechaFin) {
        this.fechaFin = fechaFin;
    }

    public List<Pago> getPagos() {
        return pagos;
    }

    public void setPagos(Pago pagos) {
        this.pagos.add(pagos);
    }
}
