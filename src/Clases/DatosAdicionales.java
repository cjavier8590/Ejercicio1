package Clases;

/**
 * Created by Javier Lopez on 3/07/2017.
 */
public class DatosAdicionales {

    public String direccion;
    public String telefono;
    public String nacionalidad;
    public String estadoCivil;



    public DatosAdicionales(String direccion, String telefono, String nacionalidad, String estadoCivil) {
        this.direccion = direccion;
        this.telefono = telefono;
        this.nacionalidad = nacionalidad;
        this.estadoCivil = estadoCivil;
    }

    public DatosAdicionales() {
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }
}
