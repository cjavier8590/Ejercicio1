package Clases;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Javier Lopez on 3/07/2017.
 */
public class Cliente {

    public String primerNombre;
    public String segundoNombre;
    public String primerApellido;
    public String segundoApellido;
    public Date fechaNacimiento;
    public String dpi;

    public List<Credito> creditos;
    public DatosAdicionales datos;


    public Cliente(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, Date fechaNacimiento, String dpi) {
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.fechaNacimiento = fechaNacimiento;
        this.dpi = dpi;
        creditos = new ArrayList<>();
    }

    public Cliente() {
        creditos = new ArrayList<>();
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDpi() {
        return dpi;
    }

    public void setDpi(String dpi) {
        this.dpi = dpi;
    }

    public void setDatos(DatosAdicionales datos) {
        this.datos = datos;
    }

    public DatosAdicionales getDatos() {
        return datos;
    }

    public List<Credito> getCreditos() {
        return creditos;
    }

    public void setCreditos(Credito creditos) {
        this.creditos.add(creditos);
    }
}
