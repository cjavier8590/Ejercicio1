package Interfaz;

import Clases.Cliente;

import javax.net.ssl.SSLContext;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class DatosAdicionales extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTable tablaDatos;
    private JTextField tDireccion;
    private JTextField tTelefono;
    private JTextField tEstado;
    private JTextField tNacionalidad;

    List<Cliente> clientes = new ArrayList<>();
    DefaultTableModel modelo = new DefaultTableModel();
    DefaultListModel listado = new DefaultListModel();
    int seleccionado;

    public void setSeleccionado(int seleccionado) {

        System.out.print(seleccionado);
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;

    }

    public int getSeleccionado() {
        return seleccionado;
    }

    public DatosAdicionales(int seleccionado) {
        this.seleccionado = seleccionado;

        if(seleccionado <= clientes.size()-1){
            tDireccion.setText(clientes.get(getSeleccionado()).getDatos().getDireccion());
            tTelefono.setText(clientes.get(getSeleccionado()).getDatos().getTelefono());
            tNacionalidad.setText(clientes.get(getSeleccionado()).getDatos().getNacionalidad());
            tEstado.setText(clientes.get(getSeleccionado()).getDatos().getEstadoCivil());
        }



        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                Clases.DatosAdicionales datos = new Clases.DatosAdicionales();
                datos.setDireccion(tDireccion.getText());
                datos.setTelefono(tTelefono.getText());
                datos.setNacionalidad(tNacionalidad.getText());
                datos.setEstadoCivil(tEstado.getText());
                clientes.get(getSeleccionado()).setDatos(datos);
                System.out.print(clientes.get(getSeleccionado()).getDatos().getDireccion());
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // add your code here

    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        DatosAdicionales dialog = new DatosAdicionales(0);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
