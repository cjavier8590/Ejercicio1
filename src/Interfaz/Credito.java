package Interfaz;

import Clases.Cliente;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Credito extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tMontoTextField;
    private JTextField tIntereses;
    private JTextField tFechaInicio;
    private JTextField tFechaFin;

    int seleccionado;
    List<Cliente> clientes = new ArrayList<>();

    public Credito(int seleccionado) {
        this.seleccionado = seleccionado;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public int getSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(int seleccionado) {
        this.seleccionado = seleccionado;
    }

    public Credito() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // add your code here
        Clases.Credito nuevo = new Clases.Credito();
        nuevo.setMonto(tMontoTextField.getText());
        nuevo.setInteres(Double.parseDouble(tIntereses.getText()));
        nuevo.setFechaInicio(Date.parse(String.valueOf(tFechaInicio)));
        nuevo.setFechaFin(Date.parse(String.valueOf(tFechaFin)));
        clientes.get(getSeleccionado()).setCreditos(nuevo);
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        Credito dialog = new Credito();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
