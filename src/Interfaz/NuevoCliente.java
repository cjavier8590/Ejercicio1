package Interfaz;

import Clases.*;
import Clases.Credito;
import Interfaz.Menu;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import static java.lang.System.out;

public class NuevoCliente extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tPrimer;
    private JTextField tSegundo;
    private JTextField tPrimerA;
    private JTextField tSegundoA;
    private JTextField tFecha;
    private JTextField tDpi;
    private JLabel label;
    private JTable tablaCliente;
    private JButton agregarCreditoButton;

    List<Cliente> clientes = new ArrayList<>();
    SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");

    DefaultTableModel modelo = new DefaultTableModel();

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
        for(int x = 0; x <= clientes.size()-1; x++){
            modelo.addRow(new Object[]{clientes.get(x).getPrimerNombre(), clientes.get(x).getSegundoNombre(), clientes.get(x).getPrimerApellido(), clientes.get(x).getSegundoApellido(), clientes.get(x).getFechaNacimiento(), clientes.get(x).getDpi()});
        }

    }

    public NuevoCliente() {

        modelo.addColumn("Primer Nombre");
        modelo.addColumn("Segundo Nombre");
        modelo.addColumn("Primer Apellido");
        modelo.addColumn("Segundo Apellido");
        modelo.addColumn("Fecha de Nacimiento");
        modelo.addColumn("DPI");

        tablaCliente.setModel(modelo);

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                Cliente nuevo = new Cliente();

                nuevo.setPrimerNombre(tPrimer.getText());

                nuevo.setSegundoNombre(tSegundo.getText());

                nuevo.setPrimerApellido(tPrimerA.getText());

                nuevo.setSegundoApellido(tSegundoA.getText());

                nuevo.setDpi(tDpi.getText());

                String fecha = tFecha.getText();
                Date date1 = null;
                try {
                    date1 = myFormat.parse(fecha);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                nuevo.setFechaNacimiento(date1);
                clientes.add(nuevo);


                modelo.addRow(new Object[]{nuevo.getPrimerNombre(), nuevo.getSegundoNombre(), nuevo.getPrimerApellido(), nuevo.getSegundoApellido(), tFecha.getText(), nuevo.getDpi()});


                JOptionPane.showMessageDialog(getParent(), "Cliente Agregado Exitosamente");



            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DatosAdicionales datos = new DatosAdicionales(tablaCliente.getSelectedRow());
                datos.setClientes(clientes);
                datos.setSize(700, 770);
                datos.setVisible(true);

            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        agregarCreditoButton.addActionListener(e -> {
            Interfaz.Credito datos = new Interfaz.Credito(tablaCliente.getSelectedRow());
            datos.setClientes(clientes);
            datos.setSize(700, 770);
            datos.setVisible(true);
            //esto se tiene que comentar
        });
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        NuevoCliente dialog = new NuevoCliente();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
