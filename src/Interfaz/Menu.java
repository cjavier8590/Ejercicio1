package Interfaz;

import Clases.Cliente;
import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class  Menu extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JButton Datos;
    private JButton clientes;

    public void setClientes_lista(List<Cliente> clientes_lista) {
        this.clientes_lista = clientes_lista;
    }

    List<Cliente> clientes_lista = new ArrayList<>();
    SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");

    public Menu() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        clientes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                NuevoCliente ventana = new NuevoCliente();
                ventana.setClientes(clientes_lista);
                ventana.setSize(700,350);
                ventana.setName("Ingreso de Clientes");
                ventana.setVisible(true);


            }
        });

    }

    private void onOK() {
        // add your code here
        JOptionPane.showMessageDialog(this, "Hola");
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        Menu dialog = new Menu();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
