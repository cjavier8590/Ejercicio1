package Interfaz;

import Clases.Cliente;
import Clases.Credito;
import Clases.DatosAdicionales;
import Clases.Pago;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.out;

/**
 * Created by Javier Lopez on 4/07/2017.
 */
public class Principal {

    public static void main(String[] args) {
        List<Cliente> clientes = new ArrayList<>();
        Scanner sn = new Scanner(System.in);
        boolean exit = false;
        int opcion;
        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");

        while(!exit){
            out.println("Seleccione una de las siguientes opciones");
            out.println("1. Ingrese un cliente");
            out.println("2. Listado de Clientes");
            out.println("3. Ingreso de Datos Adicionales");
            out.println("4. Visualizar Datos Adicionales de Clientes");
            out.println("5. Agregar Crédito a Cliente");
            out.println("7. Salir");

            try{
                opcion = sn.nextInt();
                switch (opcion){
                    case 1:
                        Cliente nuevo = new Cliente();
                        out.println("Ingrese el primer nombre");
                        nuevo.setPrimerNombre(sn.next());
                        out.println("Ingrese el primer nombre");
                        nuevo.setSegundoNombre(sn.next());
                        out.println("Ingrese el primer nombre");
                        nuevo.setPrimerApellido(sn.next());
                        out.println("Ingrese el primer nombre");
                        nuevo.setSegundoApellido(sn.next());
                        out.println("Ingrese el número de DPI");
                        nuevo.setDpi(sn.next());
                        out.println("Ingrese su fecha de nacimiento");
                        String fecha = sn.next();
                        Date date1 = myFormat.parse(fecha);
                        nuevo.setFechaNacimiento(date1);
                        clientes.add(nuevo);
                        break;

                    case 2:
                        out.println("Listado de Clientes: ");
                        int x = 0;
                        while(x <= clientes.size() - 1){
                            out.println(clientes.size());
                            out.println((x+1) + ": " + clientes.get(x).getPrimerNombre() +
                                    " " + clientes.get(x).getSegundoNombre() + " " + clientes.get(x).getPrimerApellido() + " " +
                                    clientes.get(x).segundoApellido);
                            x++;
                        }
                        break;
                    case 3:
                        out.println("Ingrese datos Adicionales para Cliente Determinado");
                        int y = 0;
                        while(y <= clientes.size() - 1){
                            out.println(clientes.size());
                            out.println((y) + ": " + clientes.get(y).getPrimerNombre() +
                                    " " + clientes.get(y).getSegundoNombre() + " " + clientes.get(y).getPrimerApellido() + " " +
                                    clientes.get(y).segundoApellido);
                            y++;
                        }

                        int opcionAdicional = sn.nextInt();
                        if(opcionAdicional <= clientes.size() - 1){
                            DatosAdicionales adicionales = new DatosAdicionales();
                            out.println("Ingrese la Direccion del Cliente");
                            adicionales.setDireccion(sn.next());
                            out.println("Ingrese el Telefono del Cliente");
                            adicionales.setTelefono(sn.next());
                            out.println("Ingrese la Nacionalidad");
                            adicionales.setNacionalidad(sn.next());
                            out.println("Ingrese el Estado Civil");
                            adicionales.setEstadoCivil(sn.next());
                            clientes.get(opcionAdicional).setDatos(adicionales);
                        }
                        break;

                    case 4:
                        out.println("Seleccione la persona de quien quiere visualizar sus datos adicionales");
                        int z = 0;
                        while(z <= clientes.size() - 1){
                            out.println(clientes.size());
                            out.println((z) + ": " + clientes.get(z).getPrimerNombre() +
                                    " " + clientes.get(z).getSegundoNombre() + " " + clientes.get(z).getPrimerApellido() + " " +
                                    clientes.get(z).segundoApellido);
                            z++;
                        }
                        int verAdicionales = sn.nextInt();
                        out.println("Direccion: " + clientes.get(verAdicionales).getDatos().getDireccion());
                        out.println("Telefono: " + clientes.get(verAdicionales).getDatos().getTelefono());
                        out.println("Nacionalidad: " + clientes.get(verAdicionales).getDatos().getNacionalidad());
                        out.println("Estado Civil: " + clientes.get(verAdicionales).getDatos().getEstadoCivil());
                        break;

                    case 5:
                        out.println("Ingrese datos Adicionales para Cliente Determinado");
                        int a = 0;
                        while(a <= clientes.size() - 1){
                            out.println(clientes.size());
                            out.println((a) + ": " + clientes.get(a).getPrimerNombre() +
                                    " " + clientes.get(a).getSegundoNombre() + " " + clientes.get(a).getPrimerApellido() + " " +
                                    clientes.get(a).segundoApellido);
                            a++;
                        }

                        int opcionCredito = sn.nextInt();
                        if(opcionCredito <= clientes.size() - 1){
                            Credito credito = new Credito();
                            out.println("Ingrese el monto del crédito");
                            credito.setMonto(sn.next());
                            out.println("Ingrese la fecha de inicio del crédito");
                            fecha = sn.next();
                            date1 = myFormat.parse(fecha);
                            //credito.setFechaInicio(date1);
                            out.println("Ingrese la fecha de finalizacion del crédito");
                            fecha = sn.next();
                            date1 = myFormat.parse(fecha);
                            //credito.setFechaInicio(date1);
                            //credito.setFechaFin(date1);
                            out.println("Ingrese la cantidad de intereses");
                            credito.setInteres(sn.nextDouble());
                            clientes.get(opcionCredito).setCreditos(credito);
                        }
                        break;
                    
                    case 6:
                        out.println("Seleccione la persona de quien quiere visualizar sus créditos");
                        int c = 0;
                        while(c <= clientes.size() - 1){
                            
                            out.println((c) + ": " + clientes.get(c).getPrimerNombre() +
                                    " " + clientes.get(c).getSegundoNombre() + " " + clientes.get(c).getPrimerApellido() + " " +
                                    clientes.get(c).segundoApellido);
                            c++;
                        }
                        int verCreditos = sn.nextInt();
                        for(int contaCreditos = 0; contaCreditos <= clientes.get(verCreditos).getCreditos().size() - 1; contaCreditos++){
                            System.out.println("El monto del crédito " + contaCreditos + " es de " + clientes.get(verCreditos).getCreditos().get(contaCreditos).getMonto());
                            System.out.println("La fecha de inicio del credito " + contaCreditos + " es  " + clientes.get(verCreditos).getCreditos().get(contaCreditos).getFechaInicio());
                            System.out.println("El fecha de finalización del crédito " + contaCreditos + " es  " + clientes.get(verCreditos).getCreditos().get(contaCreditos).getFechaFin());
                            System.out.println("El monto de interés del crédito " + contaCreditos + " es de " + clientes.get(verCreditos).getCreditos().get(contaCreditos).getInteres());
                        }
                        break;
                    case 7:
                        out.println("Seleccione al cliente para visualizar sus créditos");
                        int vCliente = 0;
                        while(vCliente <= clientes.size() - 1){
                            out.println((vCliente) + ": " + clientes.get(vCliente).getPrimerNombre() +
                                    " " + clientes.get(vCliente).getSegundoNombre() + " " + clientes.get(vCliente).getPrimerApellido() + " " +
                                    clientes.get(vCliente).segundoApellido);
                            vCliente++;
                        }
                        int opcionC = sn.nextInt();
                        int vCredito = 0;
                        out.println("Seleccione el crédito al que le quiere agregar pagos");
                        while(vCredito <= clientes.get(opcionC).getCreditos().size() - 1){
                            out.println("Monto: " + clientes.get(opcionC).getCreditos().get(vCredito).getMonto() + ", vence " +
                                    clientes.get(opcionC).getCreditos().get(vCredito).getFechaFin());
                            vCredito++;
                        }
                        int opcionCre = sn.nextInt();
                        out.println("Ingrese el numero de boleta de pago");
                        Pago pago = new Pago();
                        out.println("Ingrese el No. de Boleta de Pago");
                        pago.setNoPago(sn.next());
                        out.println("Ingrese el Banco comercial de la boleta");
                        pago.setBanco(sn.next());
                        out.println("Ingrese la fecha de pago");
                        fecha = sn.next();
                        date1 = myFormat.parse(fecha);
                        pago.setFechaPago(date1);
                        out.println("Ingrese el monto de la boleta");
                        pago.setMontoPago(sn.next());
                        clientes.get(opcionC).getCreditos().get(vCredito).setPagos(pago);
                    case 9:
                        exit = true;
                        break;
                }
            }
            catch(Exception e){
                out.println(e);
            }
        }




    }
}
